<?php

use Phinx\Migration\AbstractMigration;

class WsCoreMigration extends AbstractMigration
{ 
    public function up() 
    { 
        $table = $this->table('city'); 
        $table->addColumn('name', 'string') 
            ->addColumn('countrycode', 'string', ['limit' => 3]) 
            ->addColumn('district', 'string', ['null' => true])
            ->addColumn('population', 'integer', ['null' => true])
            ->save();

         $table = $this->table('country', ['id' => false, 'primary_key' => 'code']);
         $table->addColumn('code', 'string', ['limit' => 3]) 
            ->addColumn('name', 'string')
            ->addColumn('continent', 'string')
            ->addColumn('region', 'string')
            ->addColumn('surfacearea', 'integer')
            ->addColumn('indepyear', 'integer', ['null' => true]) 
            ->addColumn('population', 'biginteger')
            ->addColumn('lifeexpectancy', 'float', ['null' => true])
            ->addColumn('gnp', 'integer', ['null' => true]) 
            ->addColumn('gnpold', 'integer', ['null' => true])
            ->addColumn('localname', 'string', ['null' => true])
            ->addColumn('governmentform', 'string', ['null' => true]) 
            ->addColumn('headofstate', 'string', ['null' => true])
            ->addColumn('capital', 'integer', ['null' => true])  
            ->addColumn('code2', 'string', ['limit' => 2])
            ->addIndex(['code2', 'name'], ['unique' => true], ['null' => true])
            ->save();

        $table = $this->table('user'); 
        $table->addColumn('email', 'string') 
            ->addColumn('username', 'string') 
            ->addColumn('password', 'string') 
            ->addIndex(['email'], ['unique' => true]) 
            ->save(); 
    } 
    public function down() 
    { 
        $this->table('country')->drop()->save(); 
        $this->table('city')->drop()->save(); 
        $this->table('user')->drop()->save(); 
    } 
} 
