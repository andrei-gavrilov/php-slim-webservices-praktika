<?php 

use Phinx\Migration\AbstractMigration; 

class WsFkMigration extends AbstractMigration 
{ 
    public function up() 
    { 

        $password = password_hash('asdasd', PASSWORD_DEFAULT); 
        $this->query("INSERT INTO user (username, email, password) VALUES('andrei','andrei.gavrilov.kk@gmail.com', '$password')"); 

        $sql = file_get_contents('./data.sql'); 
        $this->query($sql); 

        $table = $this->table('city'); 
        $table->addForeignKey('countrycode', 'country', 'code', 
            ['delete' => 'CASCADE', 'update' => 'CASCADE']) 
            ->save(); 

    } 
    public function down() 
    { 
        $this->table('city')->drop()->save(); 
        $this->table('country')->drop()->save(); 
        $this->table('user')->drop()->save(); 
    } 
} 
