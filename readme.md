# WebServices Praktika

Realisation of RESTful webservices.



## Prerequisite
To start this  application With PHP�s Webserver run the following command from the src/public directory:
``` bash
php -S localhost:8080
```
To get this application working on a LAMP stack you'll have to add this(or similar)  record to hosts file:
```sh
127.0.0.1		slimapp
```
And set up virtual host configuration:
```sh
<VirtualHost *:80>
    DocumentRoot "E:\Your\Path\slimapp\public"
    ServerName slimapp
	<Directory "E:\Your\Path\slimapp\public">
	Allow from all
	AllowOverride all
	Require all granted
	</Directory>
</VirtualHost>
```
Then you can clone repository content(git clone https://andrei-gavrilov@bitbucket.org/andrei-gavrilov/webservices-praktika.git .) to your slimapp folder.
## Install

Install composer dependencies.

``` bash
$ composer install 
```

Create mysql database called
```sh
ws_production_db
```
You can adjust your database settings in **phinx.xml** and **src\api\settings.php** files.
To migrate database and fill it with data run 
``` bash
$ vendor\bin\phinx migrate
```
To rollback run 
``` bash
$ vendor\bin\phinx rollback
```

## Usage

### API list
+ GET:
    * api/v1/world/country/all
    * api/v1/world/country/continent/{name}
    * api/v1/world/country/{name}/city/all
    * __api/v1/world/country/{code}__
    * __api/v1/world/city/all__
    * __api/v1/world/city/{name}__

+ POST:
    * api/v1/world/city/add

+ PUT:
    * api/v1/world/city/update/{id}

+ DELETE:
    * api/v1/world/city/delete/{id}

### Authentication & authorization
+ POST 
    * signup (username, email, password)
    * signin (email, password) 


