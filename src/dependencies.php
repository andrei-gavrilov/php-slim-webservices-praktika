<?php
// DIC configuration

$container = $app->getContainer();

//  PDO
$container['pdo'] = function ($c) {
    $settings = $c->get('settings');

    $host = $settings['db']['host'];
    $dbname = $settings['db']['database'];
    $username = $settings['db']['username'];
    $password = $settings['db']['password'];
    $charset = $settings['db']['charset'];

    $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";

    $options = [
        PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION,
    ];

    return new PDO($dsn, $username, $password, $options);
};

//  Controllers
$container['APIController'] = function ($c) {
    return new SlimApp\API\Controllers\APIController($c);
};

// Validator
$container['validator'] = function () {
    return new Awurth\SlimValidation\Validator();
};
