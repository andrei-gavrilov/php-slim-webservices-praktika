<?php




namespace SlimApp\API\Controllers;
use Respect\Validation\Validator as V;
use PDO;

class APIController
{

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getCountries($request, $response)
    {
        $sql = "SELECT * FROM country";
        try {
            $db = $this->container->get('pdo');
            $stmt = $db->query($sql);
            $countries = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
        } catch (PDOException $e) {
            return $response->withJson(['error' => true, 'message' => $e->getMessage()]);
        }
        return $response->withJson($countries, 200, JSON_NUMERIC_CHECK);
    }

    public function getCountriesByContinent($request, $response, $args)
    {
        $validator = $this->container->get('validator')->validate($args, [
            'name' => V::notBlank(),
        ]);
        if ($validator->isValid()) {
            $sql = "SELECT * FROM country WHERE continent LIKE :continent";
            try {
                $db = $this->container->get('pdo');
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':continent', $args['name'], PDO::PARAM_STR);
                $stmt->execute();
                $countries = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;
            } catch (PDOException $e) {
                return $response->withJson(['error' => true, 'message' => $e->getMessage()]);
            }
            return $response->withJson($countries, 200, JSON_NUMERIC_CHECK);
        } else {
            $errors = $validator->getErrors();
            return $response->withJson($errors, 400, JSON_NUMERIC_CHECK);
        }
    }

    public function getCitiesByCountryName($request, $response, $args)
    {
        $validator = $this->container->get('validator')->validate($args, [
            'name' => V::notBlank(),
        ]);
        if ($validator->isValid()) {
            $sql = "SELECT city.* FROM city INNER JOIN country ON city.countrycode=country.code WHERE country.name=:name";
            try {
                $db = $this->container->get('pdo');
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':name', $args['name'], PDO::PARAM_STR);
                $stmt->execute();
                $cities = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;
            } catch (PDOException $e) {
                return $response->withJson(['error' => true, 'message' => $e->getMessage()]);
            }
            return $response->withJson($cities, 200, JSON_NUMERIC_CHECK);
        } else {
            $errors = $validator->getErrors();
            return $response->withJson($errors, 400, JSON_NUMERIC_CHECK);
        }
    }

    public function getCountriesByCode($request, $response, $args)
    {
        $validator = $this->container->get('validator')->validate($args, [
            'code' => V::notBlank(),
        ]);
        if ($validator->isValid()) {
            $sql = "SELECT * FROM country WHERE code=:code";
            try {
                $db = $this->container->get('pdo');
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':code', $args['code'], PDO::PARAM_STR);
                $stmt->execute();
                $country = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;
                return $response->withJson($country, 200, JSON_NUMERIC_CHECK);
            } catch (PDOException $e) {
                return $response->withJson(['error' => true, 'message' => $e->getMessage()]);
            }
            return $response->withJson($cities, 200, JSON_NUMERIC_CHECK);
        } else {
            $errors = $validator->getErrors();
            return $response->withJson($errors, 400, JSON_NUMERIC_CHECK);
        }
    }
    public function getCities($request, $response, $args)
    {
        $sql = "SELECT * FROM city";
        try {
            $db = $this->container->get('pdo');
            $stmt = $db->query($sql);
            $cities = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            return $response->withJson($cities, 200, JSON_NUMERIC_CHECK);
        } catch (PDOException $e) {
            return $response->withJson(['error' => true, 'message' => $e->getMessage()]);
        }
    }

    public function getCitiesByName($request, $response, $args)
    {
        $validator = $this->container->get('validator')->validate($args, [
            'name' => V::notBlank(),
        ]);
        if ($validator->isValid()) {
            $sql = "SELECT * FROM city WHERE name=:name";
            try {
                $db = $this->container->get('pdo');
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':name', $args['name'], PDO::PARAM_STR);
                $stmt->execute();
                $city = $stmt->fetchAll(PDO::FETCH_OBJ);
                $db = null;
                return $response->withJson($city, 200, JSON_NUMERIC_CHECK);
            } catch (PDOException $e) {
                return $response->withJson(['error' => true, 'message' => $e->getMessage()]);
            }
        } else {
            $errors = $validator->getErrors();
            return $response->withJson($errors, 400, JSON_NUMERIC_CHECK);
        }
    }

    public function addCity($request, $response)
    {
        $validator = $this->container->get('validator')->validate($request, [
            'name' => V::notBlank(),
            'countrycode' => V::notBlank(),
            'district' => V::notBlank(),
            'population' => V::numeric()->notBlank(),
        ]);
        if ($validator->isValid()) {
            $sql = "SELECT * FROM country WHERE code = :countrycode LIMIT 1";
            try {
                $db = $this->container->get('pdo');
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':countrycode', $request->getParam('countrycode'), PDO::PARAM_STR);
                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    $name = $request->getParam('name');
                    $countrycode = $request->getParam('countrycode');
                    $district = $request->getParam('district');
                    $population = $request->getParam('population');
                    $sql = "INSERT INTO city(name, countrycode, district, population) VALUES (:name,:countrycode,:district,:population)";
                    try {
                        $db = $this->container->get('pdo');
                        $stmt = $db->prepare($sql);
                        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
                        $stmt->bindParam(':countrycode', strtoupper($countrycode), PDO::PARAM_STR);
                        $stmt->bindParam(':district', $district, PDO::PARAM_STR);
                        $stmt->bindParam(':population', $population, PDO::PARAM_INT);
                        $stmt->execute();
                    } catch (PDOException $e) {
                        return $response->withJson(['error' => true, 'text' => $e->getMessage()]);
                    }
                    return $response->withJson(['notice' => true, 'text' => 'City Added']);
                } else {
                    return $response->withJson(['error' => true, 'text' => 'Country with this code does not exists']);
                }
            } catch (PDOException $e) {
                return $response->withJson(['error' => true, 'text' => $e->getMessage()]);
            }
        } else {
            $errors = $validator->getErrors();
            return $response->withJson($errors, 400, JSON_NUMERIC_CHECK);
        }
    }

    public function updateCity($request, $response, $args)
    {
        $validator = $this->container->get('validator')->validate($request, [
            'name' => V::notBlank(),
            'countrycode' => V::notBlank(),
            'district' => V::notBlank(),
            'population' => V::numeric()->notBlank(),
        ]);
        if ($validator->isValid()) {
            $sql = "SELECT * FROM country WHERE code = :countrycode LIMIT 1";
            $id = $args['id'];
            $name = $request->getParam('name');
            $countrycode = $request->getParam('countrycode');
            $district = $request->getParam('district');
            $population = $request->getParam('population');

            try {
                $db = $this->container->get('pdo');
                $stmt = $db->prepare($sql);
                $stmt->bindParam(':countrycode', $countrycode, PDO::PARAM_STR);
                $stmt->execute();
                if ($stmt->rowCount() > 0) {
                    $sql = "UPDATE city SET
                                name            = :name,
                                countrycode 	= :countrycode,
                                district		= :district,
                                population 	    = :population
                            WHERE id = :id";
                    try {
                        $db = $this->container->get('pdo');
                        $stmt = $db->prepare($sql);
                        $stmt->bindParam(':id', $id);
                        $stmt->bindParam(':name', $name);
                        $stmt->bindParam(':countrycode', $countrycode);
                        $stmt->bindParam(':district', $district);
                        $stmt->bindParam(':population', $population);
                        $stmt->execute();
                        $db = null;
                    } catch (PDOException $e) {
                        return $response->withJson(['error' => true, 'text' => $e->getMessage()]);
                    }
                    $count = $stmt->rowCount();
                    if ($count > 0) {
                        return $response->withJson(['notice' => true, 'text' => 'City Updated']);
                    } else {
                        return $response->withJson(['error' => true, 'text' => 'City with this Id does not exists or there are nothing to change']);
                    }
                } else {
                    return $response->withJson(['error' => true, 'text' => 'Country code does not exists']);
                }
            } catch (PDOException $e) {
                return $response->withJson(['error' => true, 'text' => $e->getMessage()]);
            }
        } else {
            $errors = $validator->getErrors();
            return $response->withJson($errors, 400, JSON_NUMERIC_CHECK);
        }
    }

    public function deleteCity($request, $response, $args)
    {
        $id = $request->getAttribute('id');
        $sql = "DELETE FROM city WHERE id = :id";
        try {
            $db = $this->container->get('pdo');
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $count = $stmt->rowCount();
            $db = null;
            if ($count > 0) {
                return $response->withJson(['error' => true, 'text' => 'City Deleted']);
            } else {
                return $response->withJson(['notice' => true, 'text' => 'City with this Id does not exists']);
            }
        } catch (PDOException $e) {
            return $response->withJson(['error' => true, 'text' => $e->getMessage()]);
        }
    }
}
