<?php

use Slim\Http\Response;

$app->get('/api/v1/world/country/all', 'APIController:getCountries');
$app->get('/api/v1/world/country/continent/{name}', 'APIController:getCountriesByContinent');
$app->get('/api/v1/world/country/{name}/city/all', 'APIController:getCitiesByCountryName');
$app->get('/api/v1/world/country/{code}', 'APIController:getCountriesByCode');

$app->get('/api/v1/world/city/all', 'APIController:getCities');
$app->get('/api/v1/world/city/{name}', 'APIController:getCitiesByName');
$app->post('/api/v1/world/city/add', 'APIController:addCity');
$app->put('/api/v1/world/city/update/{id}', 'APIController:updateCity');
$app->delete('/api/v1/world/city/delete/{id}', 'APIController:deleteCity');
