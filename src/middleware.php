<?php
// Application middleware

use Tuupola\Middleware\JwtAuthentication;

$app->add(new Tuupola\Middleware\JwtAuthentication([
    "path" => ["/api/v1/world/city"],
    "ignore" => ["/api/v1/world/city/all", "/api/v1/world/city/{name}"],
    "attribute" => "decoded_token_data",
    "cookie" => "token",
    "secure" => false,
    "relaxed" => ["localhost", "slimapp"],
    "secret" => "supersecretkeyyoushouldnotcommittogithub",
    "algorithm" => ["HS256"],
    "error" => function ($response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    },
]));
