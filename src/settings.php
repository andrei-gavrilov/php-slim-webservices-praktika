<?php
return [
    'settings' => [
        'displayErrorDetails' => false, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        'db' => [
            'host' => 'localhost',
            'username' => 'root',
            'password' => '',
            'database' => 'ws_production_db',
            'charset' => 'utf8',
        ],
        'jwt' => [
            'secret' => 'supersecretkeyyoushouldnotcommittogithub',
        ],
    ],
];
