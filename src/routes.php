<?php

use Firebase\JWT\JWT;
use Respect\Validation\Validator as V;
use Slim\Http\Request;
use Slim\Http\Response;
// Routes






$app->post('/signup', function (Request $request, Response $response) {
    $validator = $this->get('validator')->validate($request, [
        'username' => V::notBlank(),
        'email' => V::email()->notBlank(),
        'password' => V::notBlank(),
    ]);
    if ($validator->isValid()) {
        $username = $request->getParam('username');
        $email = $request->getParam('email');
        $password = password_hash($request->getParam('password'), PASSWORD_DEFAULT);

        $sql = "SELECT * FROM user WHERE email= :email";
        try {
            $db = $this->get('pdo');
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $user = $stmt->execute();
            $db = null;
        } catch (PDOException $e) {
            return $response->withJson(['error' => true, 'message' => $e->getMessage()], 503);
        }
        if ($stmt->rowCount() > 0) {
            return $response->withJson(['error' => true, 'message' => 'This email is already taken'], 422);
        }

        $sql = "INSERT INTO user(username, email, password) VALUES(:username, :email, :password)";
        try {
            $db = $this->get('pdo');
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':username', $username, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':password', $password, PDO::PARAM_STR);
            $stmt->execute();
            $db = null;
        } catch (PDOException $e) {
            return $response->withJson(['error' => true, 'message' => $e->getMessage()], 503);
        }
        return $response->withJson(['notice' => true, 'message' => 'You have successfully registered']);
    } else {
        $errors = $validator->getErrors();
        return $response->withJson($errors, 400, JSON_NUMERIC_CHECK);
    }
});

$app->post('/signin', function (Request $request, Response $response) {
    $input = $request->getParsedBody();
    $validator = $this->get('validator')->validate($request, [
        'email' => V::email()->notBlank(),
        'password' => V::notBlank(),
    ]);
    if ($validator->isValid()) {
        $sql = "SELECT * FROM user WHERE email= :email";

        try {
            $db = $this->get('pdo');
            $stmt = $db->prepare($sql);
            $stmt->bindParam("email", $input['email']);
            $stmt->execute();
            $user = $stmt->fetchObject();
            $db = null;
        } catch (PDOException $e) {
            return $response->withJson(['error' => true, 'message' => $e->getMessage()], 503);
        }

        if (!$user) {
            return $response->withJson(['error' => true, 'message' => 'These credentials do not match our records.'], 401);
        }
        if (!password_verify($input['password'], $user->password)) {
            return $response->withJson(['error' => true, 'message' => 'These credentials do not match our records.'], 401);
        }

        $settings = $this->get('settings');
        $token = JWT::encode(['id' => $user->id, 'name' => $user->username, 'email' => $user->email], $settings['jwt']['secret'], "HS256");
        return $response->withJson(['token' => $token]);
    } else {
        $errors = $validator->getErrors();
        return $response->withJson($errors, 400, JSON_NUMERIC_CHECK);
    }
});
