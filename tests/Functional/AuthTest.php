<?php

namespace Tests\Functional;

class AuthTest extends BaseTestCase
{
    /** @test */
    public function SignUp_EmailTaken_True()
    {
        $response = $this->runApp('POST', '/signup', ['email' => "andrei.gavrilov.kk@gmail.com", 'password' => "asdasd", 'username' => "asdasd"]);
        $this->assertContains('This email is already taken', (string) $response->getBody());
    }

    /** @test */
    public function SignUp_MissingArgument_True()
    {
        $response = $this->runApp('POST', '/signup', ['email' => "andrei.gavrilov.kk@gmail.com", 'username' => "asdasd"]);
        $this->assertContains('must not be blank', (string) $response->getBody());
    }

    /** @test */
    public function SignIn_WrongPassword_True()
    {
        $response = $this->runApp('POST', '/signin', ['email' => "andrei.gavrilov.kk@gmail.com", 'password' => "fffasd"]);
        $this->assertContains('These credentials do not match our records', (string) $response->getBody());
    }

    /** @test */
    public function SignIn_MissingArgument_True()
    {
        $response = $this->runApp('POST', '/signin', ['email' => "andrei.gavrilov.kk@gmail.com"]);
        $this->assertContains('must not be blank', (string) $response->getBody());
    }
    /** @test */
    public function AddCity_NoToken_True()
    {
        $response = $this->runApp('POST', '/api/v1/world/city/add', ['name' => "aleppo", 'countrycode' => "est", 'district' => "Kabul", 'population' => 1237500]);
        $this->assertContains('Token not found', (string) $response->getBody());
    }
}
